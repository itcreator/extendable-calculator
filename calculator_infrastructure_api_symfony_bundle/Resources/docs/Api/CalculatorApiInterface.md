# Calculator\InfrastructureApiSymfonyBundle\Api\CalculatorApiInterface

All URIs are relative to *http://localhost:8000/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**evaluateExpression**](CalculatorApiInterface.md#evaluateExpression) | **POST** /expression/evaluate | Evaluate expression


## Service Declaration
```yaml
# config/services.yml
services:
    # ...
    Acme\MyBundle\Api\CalculatorApi:
        tags:
            - { name: "calculator_infrastructure_api_symfony.api", api: "calculator" }
    # ...
```

## **evaluateExpression**
> Calculator\InfrastructureApiSymfonyBundle\Dto\ExpressionEvaluatedSuccessfullySchema evaluateExpression($evaluateExpressionSchema)

Evaluate expression

Endponint evaluate expressions. E.g. `1+2*3-4/2`

### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/CalculatorApiInterface.php

namespace Acme\MyBundle\Api;

use Calculator\InfrastructureApiSymfonyBundle\Api\CalculatorApiInterface;

class CalculatorApi implements CalculatorApiInterface
{

    // ...

    /**
     * Implementation of CalculatorApiInterface#evaluateExpression
     */
    public function evaluateExpression(EvaluateExpressionSchema $evaluateExpressionSchema, &$responseCode, array &$responseHeaders): array|\Calculator\InfrastructureApiSymfonyBundle\Dto\ExpressionEvaluatedSuccessfullySchema
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **evaluateExpressionSchema** | [**Calculator\InfrastructureApiSymfonyBundle\Dto\EvaluateExpressionSchema**](../Model/EvaluateExpressionSchema.md)| Data required to evaluate expressions |

### Return type

[**Calculator\InfrastructureApiSymfonyBundle\Dto\ExpressionEvaluatedSuccessfullySchema**](../Model/ExpressionEvaluatedSuccessfullySchema.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, application/problem+json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

