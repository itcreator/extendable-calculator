<?php
/**
 * ExpressionEvaluatedSuccessfullySchema
 *
 * PHP version 8.1.1
 *
 * @category Class
 * @package  Calculator\InfrastructureApiSymfonyBundle\Dto
 * @author   OpenAPI Generator team
 * @link     https://github.com/openapitools/openapi-generator
 */

/**
 * Calculator / Math parser
 *
 * Provides RPC
 *
 * The version of the OpenAPI document: 0.1.0-dev
 * 
 * Generated by: https://github.com/openapitools/openapi-generator.git
 *
 */

/**
 * NOTE: This class is auto generated by the openapi generator program.
 * https://github.com/openapitools/openapi-generator
 * Do not edit the class manually.
 */

namespace Calculator\InfrastructureApiSymfonyBundle\Dto;

use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\SerializedName;

/**
 * Class representing the ExpressionEvaluatedSuccessfullySchema model.
 *
 * @package Calculator\InfrastructureApiSymfonyBundle\Dto
 * @author  OpenAPI Generator team
 */
class ExpressionEvaluatedSuccessfullySchema 
{
        /**
     * @var float
     * @SerializedName("result")
     * @Assert\NotNull()
     * @Assert\Type("float")
     * @Type("float")
     */
    protected $result;

    /**
     * Constructor
     * @param mixed[] $data Associated array of property values initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->result = isset($data['result']) ? $data['result'] : null;
    }

    /**
     * Gets result.
     *
     * @return float
     */
    public function getResult(): float
    {
        return $this->result;
    }

    /**
     * Sets result.
     *
     * @param float $result
     *
     * @return $this
     */
    public function setResult(float $result)
    {
        $this->result = $result;

        return $this;
    }
}


