<?php
/**
 * CalculatorApiInterface
 *
 * PHP version 8.1.1
 *
 * @category Class
 * @package  Calculator\InfrastructureApiSymfonyBundle
 * @author   OpenAPI Generator team
 * @link     https://github.com/openapitools/openapi-generator
 */

/**
 * Calculator / Math parser
 *
 * Provides RPC
 *
 * The version of the OpenAPI document: 0.1.0-dev
 * 
 * Generated by: https://github.com/openapitools/openapi-generator.git
 *
 */

/**
 * NOTE: This class is auto generated by the openapi generator program.
 * https://github.com/openapitools/openapi-generator
 * Do not edit the class manually.
 */

namespace Calculator\InfrastructureApiSymfonyBundle\Api;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Calculator\InfrastructureApiSymfonyBundle\Dto\EvaluateExpressionSchema;
use Calculator\InfrastructureApiSymfonyBundle\Dto\ExpressionEvaluatedSuccessfullySchema;
use Calculator\InfrastructureApiSymfonyBundle\Dto\ProblemDetailSchema;

/**
 * CalculatorApiInterface Interface Doc Comment
 *
 * @category Interface
 * @package  Calculator\InfrastructureApiSymfonyBundle\Api
 * @author   OpenAPI Generator team
 * @link     https://github.com/openapitools/openapi-generator
 */
interface CalculatorApiInterface
{

    /**
     * Operation evaluateExpression
     *
     * Evaluate expression
     *
     * @param  \Calculator\InfrastructureApiSymfonyBundle\Dto\EvaluateExpressionSchema $evaluateExpressionSchema  Data required to evaluate expressions (required)
     * @param  \array   $responseHeaders  Additional HTTP headers to return with the response ()
     *
     * @return \Calculator\InfrastructureApiSymfonyBundle\Dto\ExpressionEvaluatedSuccessfullySchema
     */
    public function evaluateExpression(EvaluateExpressionSchema $evaluateExpressionSchema, &$responseCode, array &$responseHeaders): array|\Calculator\InfrastructureApiSymfonyBundle\Dto\ExpressionEvaluatedSuccessfullySchema;

}
