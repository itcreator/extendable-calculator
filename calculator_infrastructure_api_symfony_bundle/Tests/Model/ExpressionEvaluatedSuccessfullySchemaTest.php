<?php
/**
 * ExpressionEvaluatedSuccessfullySchemaTest
 *
 * PHP version 8.1.1
 *
 * @category Class
 * @package  Calculator\InfrastructureApiSymfonyBundle\Tests\Model
 * @author   openapi-generator contributors
 * @link     https://github.com/openapitools/openapi-generator
 */

/**
 * Calculator / Math parser
 *
 * Provides RPC
 *
 * The version of the OpenAPI document: 0.1.0-dev
 * 
 * Generated by: https://github.com/openapitools/openapi-generator.git
 *
 */

/**
 * NOTE: This class is auto generated by the openapi generator program.
 * https://github.com/openapitools/openapi-generator
 * Please update the test case below to test the model.
 */

namespace Calculator\InfrastructureApiSymfonyBundle\Dto;

use PHPUnit\Framework\TestCase;

/**
 * ExpressionEvaluatedSuccessfullySchemaTest Class Doc Comment
 *
 * @category    Class */
// * @description ExpressionEvaluatedSuccessfullySchema
/**
 * @package     Calculator\InfrastructureApiSymfonyBundle\Tests\Model
 * @author      openapi-generator contributors
 * @link        https://github.com/openapitools/openapi-generator
 */
class ExpressionEvaluatedSuccessfullySchemaTest extends TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass(): void
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp(): void
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown(): void
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass(): void
    {
    }

    /**
     * Test "ExpressionEvaluatedSuccessfullySchema"
     */
    public function testExpressionEvaluatedSuccessfullySchema()
    {
        $testExpressionEvaluatedSuccessfullySchema = new ExpressionEvaluatedSuccessfullySchema();
    }

    /**
     * Test attribute "result"
     */
    public function testPropertyResult()
    {
    }
}
