#!/usr/bin/env bash

set -ex

pwd

mkdir -p calculator_domain/var/cache

ln -sf ../../devops/scripts/pre-commit-check.sh .git/hooks/pre-commit

docker compose build --pull

docker compose run --rm -w /code/calculator_domain server composer install

docker compose run --rm -w /code/calculator_app_symfony server composer install

