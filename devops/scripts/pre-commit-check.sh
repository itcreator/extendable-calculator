#!/usr/bin/env bash

set -ex

docker compose run --rm -w /code/calculator_domain server vendor/bin/php-cs-fixer fix -v
docker compose run --rm -w /code/calculator_domain server vendor/bin/phpstan analyze -v
docker compose run --rm -w /code/calculator_domain server vendor/bin/phpunit

docker compose run --rm -w /code/calculator_app_symfony server vendor/bin/php-cs-fixer fix -v
docker compose run --rm -w /code/calculator_app_symfony server vendor/bin/phpstan analyze -v
