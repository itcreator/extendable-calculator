<?php

namespace Calculator\AppSymfony\Api;

use Calculator\Domain\Expression\Exception\WrongValueException;
use Calculator\Domain\Parser\Exception\ParsingException;
use Calculator\Domain\Parser\Exception\UnknownTokenException;
use Calculator\Domain\Parser\Exception\WrongTypeOfTokenException;
use Calculator\Domain\Parser\Lexer;
use Calculator\Domain\Parser\Parser;
use Calculator\InfrastructureApiSymfonyBundle\Api\CalculatorApiInterface;
use Calculator\InfrastructureApiSymfonyBundle\Dto\EvaluateExpressionSchema;
use Calculator\InfrastructureApiSymfonyBundle\Dto\ExpressionEvaluatedSuccessfullySchema;

class CalculatorApi implements CalculatorApiInterface
{
    public function __construct(
        private readonly Lexer $lexer,
        private readonly Parser $parser,
    ) {
    }

    /**
     * @param int $responseCode
     * @param array<string, string> $responseHeaders
     *
     * @return array<string, mixed>|ExpressionEvaluatedSuccessfullySchema
     *
     * @throws ParsingException
     * @throws UnknownTokenException
     * @throws WrongTypeOfTokenException
     * @throws WrongValueException
     * @phpstan-ignore-next-line It just implements 3-rd party interface
     */
    final public function evaluateExpression(EvaluateExpressionSchema $evaluateExpressionSchema, &$responseCode, array &$responseHeaders): array|ExpressionEvaluatedSuccessfullySchema
    {
        $tokens = $this->lexer->tokenize($evaluateExpressionSchema->getExpression());
        $expressionTree = $this->parser->parse($tokens);
        $result = $expressionTree->toFloat();

        $responseCode = 200;

        return new ExpressionEvaluatedSuccessfullySchema(['result' => $result]);
    }
}
