<?php

return [
    Symfony\Bundle\FrameworkBundle\FrameworkBundle::class => ['all' => true],
    Symfony\Bundle\TwigBundle\TwigBundle::class => ['dev' => true, 'test' => true],
    Symfony\Bundle\WebProfilerBundle\WebProfilerBundle::class => ['dev' => true, 'test' => true],
    JMS\SerializerBundle\JMSSerializerBundle::class => ['all' => true],
    Calculator\InfrastructureApiSymfonyBundle\CalculatorInfrastructureApiSymfonyBundle::class => ['all' => true],
];
