<?php

use PhpCsFixer\Config;
use PhpCsFixer\Finder;

$rules = [
    '@Symfony' => true,
    'phpdoc_align' => false,
    'array_syntax' => ['syntax' => 'short'],
    'no_extra_blank_lines' => [
        'tokens' => [
            'continue', 'curly_brace_block', 'default', 'extra', 'parenthesis_brace_block', 'square_brace_block', 'switch', 'throw', 'use',
        ],
    ],
];

$project_path = getcwd();
$finder = Finder::create()
    ->in([
        $project_path . '/src',
    ])
;

$config = new Config();

return $config
    ->setFinder($finder)
    ->setRules($rules)
    ->setCacheFile(__DIR__ . '/var/cache/.php-cs-fixer.cache')
;
