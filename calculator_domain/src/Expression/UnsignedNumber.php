<?php

namespace Calculator\Domain\Expression;

use Calculator\Domain\Expression\Exception\WrongValueException;

class UnsignedNumber implements ValueInterface
{
    /**
     * @throws WrongValueException
     */
    public function __construct(private readonly float $value)
    {
        if ($this->value < 0) {
            throw new WrongValueException("Wrong value: {$this->value}");
        }
    }

    final public function execute(): ExpressionInterface
    {
        return clone $this;
    }

    final public function toFloat(): float
    {
        return $this->value;
    }
}
