<?php

namespace Calculator\Domain\Expression\Exception;

use Calculator\Domain\BusinessException;

class EvaluationException extends BusinessException
{
}
