<?php

namespace Calculator\Domain\Expression\Exception;

use Calculator\Domain\BusinessException;

class WrongValueException extends BusinessException
{
}
