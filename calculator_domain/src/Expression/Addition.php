<?php

namespace Calculator\Domain\Expression;

class Addition implements ExpressionInterface
{
    public function __construct(
        private readonly ExpressionInterface $first,
        private readonly ExpressionInterface $second,
    ) {
    }

    final public function execute(): ExpressionInterface
    {
        return new Number($this->first->toFloat() + $this->second->toFloat());
    }

    final public function toFloat(): float
    {
        return $this->execute()->toFloat();
    }
}
