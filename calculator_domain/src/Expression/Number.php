<?php

namespace Calculator\Domain\Expression;

class Number implements ValueInterface
{
    public function __construct(private readonly float $value)
    {
    }

    final public function execute(): ExpressionInterface
    {
        return clone $this;
    }

    final public function toFloat(): float
    {
        return $this->value;
    }
}
