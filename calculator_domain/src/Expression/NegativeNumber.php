<?php

namespace Calculator\Domain\Expression;

class NegativeNumber implements ExpressionInterface
{
    public function __construct(private readonly ValueInterface $value)
    {
    }

    final public function execute(): ExpressionInterface
    {
        return clone $this;
    }

    final public function toFloat(): float
    {
        return $this->value->toFloat() * -1;
    }
}
