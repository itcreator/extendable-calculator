<?php

namespace Calculator\Domain\Expression;

interface ExpressionInterface
{
    public function execute(): ExpressionInterface;

    public function toFloat(): float;
}
