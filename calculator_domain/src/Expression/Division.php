<?php

namespace Calculator\Domain\Expression;

use Calculator\Domain\Expression\Exception\EvaluationException;

class Division implements ExpressionInterface
{
    public function __construct(
        private readonly ExpressionInterface $first,
        private readonly ExpressionInterface $second,
    ) {
    }

    /**
     * @throws EvaluationException
     */
    final public function execute(): ExpressionInterface
    {
        try {
            return new Number($this->first->toFloat() / $this->second->toFloat());
        } catch (\DivisionByZeroError $t) {
            throw new EvaluationException("Can't evaluate `{$this->first->toFloat()} / {$this->second->toFloat()}`", 0, $t);
        }
    }

    final public function toFloat(): float
    {
        return $this->execute()->toFloat();
    }
}
