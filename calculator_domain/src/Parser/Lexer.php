<?php

namespace Calculator\Domain\Parser;

use Calculator\Domain\Parser\Exception\UnknownTokenException;
use Calculator\Domain\Parser\Exception\WrongTypeOfDefinitionException;
use Calculator\Domain\Parser\Token\Definition\DefinitionInterface;
use Calculator\Domain\Parser\Token\TokenInterface;

class Lexer
{
    /**
     * @param array<DefinitionInterface> $tokenDefinitions
     *
     * @throws WrongTypeOfDefinitionException
     */
    public function __construct(private readonly array $tokenDefinitions)
    {
        foreach ($this->tokenDefinitions as $definition) {
            /* @phpstan-ignore-next-line */
            if (!$definition instanceof DefinitionInterface) {
                throw new WrongTypeOfDefinitionException();
            }
        }
    }

    /**
     * @param string $data math expression
     *
     * @return array<TokenInterface>
     *
     * @throws UnknownTokenException
     */
    final public function tokenize(string $data): array
    {
        /** @var array<TokenInterface> $tokens */
        $tokens = [];
        $index = 0;

        while ($index < strlen($data)) {
            $token = $this->findMatchingToken(substr($data, $index));

            // If no tokens were matched, it means that the string has invalid tokens
            // for which we did not define a token definition
            if (false === $token) {
                throw new UnknownTokenException($data, $index);
            }

            $tokens[] = $token;

            $index += mb_strlen($token->getValue());
        }

        return $tokens;
    }

    private function findMatchingToken(string $data): TokenInterface|false
    {
        foreach ($this->tokenDefinitions as $definition) {
            $token = $definition->match($data);

            // Return the first token that was matched.
            if ($token instanceof TokenInterface) {
                return $token;
            }
        }

        return false;
    }
}
