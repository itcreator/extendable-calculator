<?php

namespace Calculator\Domain\Parser;

use Calculator\Domain\Expression\Exception\WrongValueException;
use Calculator\Domain\Expression\ExpressionInterface;
use Calculator\Domain\Expression\NegativeNumber;
use Calculator\Domain\Expression\UnsignedNumber;
use Calculator\Domain\Parser\Exception\ParsingException;
use Calculator\Domain\Parser\Exception\WrongTypeOfTokenException;
use Calculator\Domain\Parser\Token\BinaryOperatorInterface;
use Calculator\Domain\Parser\Token\NegativeNumberToken;
use Calculator\Domain\Parser\Token\SubtractionToken;
use Calculator\Domain\Parser\Token\TokenInterface;
use Calculator\Domain\Parser\Token\ValueTokenInterface;
use SplStack;

class Parser
{
    /**
     * @param array<TokenInterface> $tokens
     *
     * @return ExpressionInterface Expression tree
     *
     * @throws ParsingException
     * @throws WrongTypeOfTokenException
     * @throws WrongValueException
     */
    final public function parse(array $tokens): ExpressionInterface
    {
        $this->assertTokensType($tokens);

        /** @var SplStack<ExpressionInterface> $operands */
        $operands = new SplStack();
        $tokens = $this->convertToReversePolishNotation($tokens);

        foreach ($tokens as $token) {
            if ($token instanceof BinaryOperatorInterface) {
                $second = $operands->pop();
                $first = $operands->pop();
                /** @var ExpressionInterface $expression */
                $expression = new ($token->getExpressionClass())($first, $second);
                $operands->push($expression);
            } elseif ($token instanceof NegativeNumberToken) {
                $operands->push(new NegativeNumber(new UnsignedNumber((float) $token->getValue())));
            } else {
                /** @var ExpressionInterface $expression */
                $expression = new ($token->getExpressionClass())((float) $token->getValue());
                $operands->push($expression);
            }
        }

        $this->ensureThereIsOneOperand($operands);

        return $operands->pop();
    }

    /**
     * @param array<TokenInterface> $tokens
     *
     * @throws WrongTypeOfTokenException
     */
    private function assertTokensType(array $tokens): void
    {
        foreach ($tokens as $token) {
            if (!$token instanceof TokenInterface) {
                throw new WrongTypeOfTokenException();
            }
        }
    }

    /**
     * @param SplStack<ExpressionInterface> $operands
     *
     * @throws ParsingException
     */
    private function ensureThereIsOneOperand(SplStack $operands): void
    {
        if (1 !== $operands->count()) {
            throw new ParsingException("Can't parse exception.");
        }
    }

    /**
     * @param array<TokenInterface> $tokens
     */
    private function isNegativeNumber(array $tokens, int $index): bool
    {
        return
            $tokens[$index] instanceof SubtractionToken
            && (!array_key_exists($index - 1, $tokens) || $tokens[$index - 1] instanceof BinaryOperatorInterface)
        ;
    }

    /**
     * @throws ParsingException
     */
    private function createNegativeNumberToken(TokenInterface $token): NegativeNumberToken
    {
        if ($token instanceof ValueTokenInterface) {
            return new NegativeNumberToken($token->getValue());
        }

        throw new ParsingException("Can't convert to Reverse Polish Notation.");
    }

    /**
     * @param SplStack<TokenInterface> $operatorStack
     *
     * @return array<TokenInterface>
     */
    private function popPrioritizedOperations(SplStack $operatorStack, BinaryOperatorInterface $token): array
    {
        /** @var array<TokenInterface> $output */
        $output = [];

        while (!$operatorStack->isEmpty()) {
            /** @var BinaryOperatorInterface $operator */
            $operator = $operatorStack->top();
            if ($operator->getPrecedence() >= $token->getPrecedence()) {
                $output[] = $operatorStack->pop();
            } else {
                break;
            }
        }

        return $output;
    }

    /**
     * @param SplStack<TokenInterface> $operatorStack
     * @param array<TokenInterface> $output
     *
     * @return array<TokenInterface>
     */
    private function popOperations(SplStack $operatorStack, array $output): array
    {
        while (!$operatorStack->isEmpty()) {
            $output[] = $operatorStack->pop();
        }

        return $output;
    }

    /**
     * @param array<TokenInterface> $tokens
     *
     * @return array<TokenInterface>
     *
     * @throws ParsingException
     */
    private function convertToReversePolishNotation(array $tokens): array
    {
        /** @var SplStack<TokenInterface> $operatorStack */
        $operatorStack = new SplStack();
        $output = [];
        $index = -1;
        $maxIndex = count($tokens) - 1;

        while (++$index <= $maxIndex) {
            $token = $tokens[$index];
            if ($this->isNegativeNumber($tokens, $index)) {
                ++$index;
                $token = $tokens[$index];

                $output[] = $this->createNegativeNumberToken($token);
            } elseif ($token instanceof BinaryOperatorInterface) {
                array_push($output, ...$this->popPrioritizedOperations($operatorStack, $token));

                $operatorStack->push($token);
            } else {
                $output[] = $token;
            }
        }

        return $this->popOperations($operatorStack, $output);
    }
}
