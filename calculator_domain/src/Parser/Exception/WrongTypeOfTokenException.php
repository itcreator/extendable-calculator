<?php

namespace Calculator\Domain\Parser\Exception;

use Calculator\Domain\BusinessException;

class WrongTypeOfTokenException extends BusinessException
{
}
