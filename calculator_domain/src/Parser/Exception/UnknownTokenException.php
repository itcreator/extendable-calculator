<?php

namespace Calculator\Domain\Parser\Exception;

use Calculator\Domain\BusinessException;
use Throwable;

class UnknownTokenException extends BusinessException
{
    public function __construct(string $expression = '', int $offset = 0, Throwable $previous = null)
    {
        $message = "Unknown token. Expression: {$expression}, offset: {$offset}";

        parent::__construct($message, 1, $previous);
    }
}
