<?php

namespace Calculator\Domain\Parser\Exception;

use Calculator\Domain\BusinessException;

class ParsingException extends BusinessException
{
}
