<?php

namespace Calculator\Domain\Parser\Token;

use Calculator\Domain\Expression\Multiplication;

class MultiplicationToken extends AbstractToken implements BinaryOperatorInterface
{
    final public function getPrecedence(): int
    {
        return 20;
    }

    final public function getExpressionClass(): string
    {
        return Multiplication::class;
    }
}
