<?php

namespace Calculator\Domain\Parser\Token;

use Calculator\Domain\Expression\NegativeNumber;

class NegativeNumberToken extends AbstractToken implements ValueTokenInterface
{
    final public function getExpressionClass(): string
    {
        return NegativeNumber::class;
    }
}
