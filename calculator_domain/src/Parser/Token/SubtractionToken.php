<?php

namespace Calculator\Domain\Parser\Token;

use Calculator\Domain\Expression\Subtraction;

class SubtractionToken extends AbstractToken implements BinaryOperatorInterface
{
    final public function getPrecedence(): int
    {
        return 10;
    }

    final public function getExpressionClass(): string
    {
        return Subtraction::class;
    }
}
