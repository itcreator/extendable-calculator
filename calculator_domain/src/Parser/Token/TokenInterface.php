<?php

namespace Calculator\Domain\Parser\Token;

interface TokenInterface
{
    public function __construct(string $value);

    public function getValue(): string;

    public function getExpressionClass(): string;
}
