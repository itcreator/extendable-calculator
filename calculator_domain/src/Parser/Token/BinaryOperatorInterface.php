<?php

namespace Calculator\Domain\Parser\Token;

interface BinaryOperatorInterface extends TokenInterface
{
    public function getPrecedence(): int;
}
