<?php

namespace Calculator\Domain\Parser\Token;

use Calculator\Domain\Expression\UnsignedNumber;

class UIntToken extends AbstractToken implements ValueTokenInterface
{
    final public function getExpressionClass(): string
    {
        return UnsignedNumber::class;
    }
}
