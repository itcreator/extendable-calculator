<?php

namespace Calculator\Domain\Parser\Token;

abstract class AbstractToken implements TokenInterface
{
    public function __construct(private readonly string $value)
    {
    }

    final public function getValue(): string
    {
        return $this->value;
    }
}
