<?php

namespace Calculator\Domain\Parser\Token\Definition\Matcher;

use Calculator\Domain\BusinessException;

class TokenMatchingException extends BusinessException
{
}
