<?php

namespace Calculator\Domain\Parser\Token\Definition\Matcher;

class TokenValueMatcher
{
    /**
     * @param string $data string that should be matched
     * @param string $pattern Regular expression
     *
     * @throws TokenMatchingException
     */
    final public function match(string $data, string $pattern): string|false
    {
        try {
            $result = preg_match($pattern, $data, $matches, PREG_OFFSET_CAPTURE);

            if (false === $result) {
                throw new TokenMatchingException(preg_last_error_msg(), preg_last_error());
            }
        } catch (\Throwable $e) {
            throw new TokenMatchingException('Regular expression error', 0, $e);
        }

        // 0 means no match was found
        if (0 === $result) {
            return false;
        }

        [$value, $offset] = $matches[0];

        return 0 !== $offset ? false : $value;
    }
}
