<?php

namespace Calculator\Domain\Parser\Token\Definition;

use Calculator\Domain\Parser\Token\TokenInterface;

interface DefinitionInterface
{
    public function match(string $data): TokenInterface|false;
}
