<?php

namespace Calculator\Domain\Parser\Token\Definition;

use Calculator\Domain\Parser\Token\Definition\Matcher\TokenValueMatcher;
use Calculator\Domain\Parser\Token\UIntToken;

class UIntDefinition implements DefinitionInterface
{
    private const PATTERN = '/\d+/';

    public function __construct(private readonly TokenValueMatcher $matcher)
    {
    }

    /**
     * @param string $data string that should be matched
     *
     * @throws Matcher\TokenMatchingException
     */
    final public function match(string $data): UIntToken|false
    {
        $result = $this->matcher->match($data, self::PATTERN);

        return false === $result ? false : new UIntToken($result);
    }
}
