<?php

namespace Calculator\Domain\Parser\Token\Definition;

use Calculator\Domain\Parser\Token\Definition\Matcher\TokenValueMatcher;
use Calculator\Domain\Parser\Token\DivisionToken;

class DivisionDefinition implements DefinitionInterface
{
    private const PATTERN = '/\//';

    public function __construct(private readonly TokenValueMatcher $matcher)
    {
    }

    /**
     * @param string $data string that should be matched
     *
     * @throws Matcher\TokenMatchingException
     */
    final public function match(string $data): DivisionToken|false
    {
        $result = $this->matcher->match($data, self::PATTERN);

        return false === $result ? false : new DivisionToken($result);
    }
}
