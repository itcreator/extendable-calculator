<?php

namespace Calculator\DomainTest\Unit\Expression;

use Calculator\Domain\Expression\NegativeNumber;
use Calculator\Domain\Expression\UnsignedNumber;
use function PHPUnit\Framework\assertEquals;
use function PHPUnit\Framework\assertInstanceOf;
use PHPUnit\Framework\TestCase;

class NegativeNumberTest extends TestCase
{
    final public function testExecute(): void
    {
        $expression = new NegativeNumber(new UnsignedNumber(2));

        $result = $expression->execute();

        assertInstanceOf(NegativeNumber::class, $result);
        assertEquals(-2, $result->toFloat());
        assertEquals(-2, $expression->toFloat());
    }
}
