<?php

namespace Calculator\DomainTest\Unit\Expression;

use Calculator\Domain\Expression\Division;
use Calculator\Domain\Expression\Exception\EvaluationException;
use Calculator\Domain\Expression\Exception\WrongValueException;
use Calculator\Domain\Expression\Number;
use Calculator\Domain\Expression\UnsignedNumber;
use function PHPUnit\Framework\assertEquals;
use function PHPUnit\Framework\assertInstanceOf;
use PHPUnit\Framework\TestCase;

class DivisionTest extends TestCase
{
    /**
     * @throws EvaluationException
     * @throws WrongValueException
     */
    final public function testExecute(): void
    {
        $expression = new Division(new UnsignedNumber(3), new UnsignedNumber(2));

        $result = $expression->execute();

        assertInstanceOf(Number::class, $result);
        assertEquals(1.5, $result->toFloat());
        assertEquals(1.5, $expression->toFloat());
    }

    /**
     * @throws WrongValueException
     */
    final public function testExecuteWithDivisionByZero(): void
    {
        $this->expectException(EvaluationException::class);
        $this->expectExceptionMessage("Can't evaluate `3 / 0`");

        $expression = new Division(new UnsignedNumber(3), new UnsignedNumber(0));

        $expression->execute();
    }
}
