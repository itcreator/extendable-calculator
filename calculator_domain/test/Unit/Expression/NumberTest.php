<?php

namespace Calculator\DomainTest\Unit\Expression;

use Calculator\Domain\Expression\Number;
use function PHPUnit\Framework\assertEquals;
use function PHPUnit\Framework\assertInstanceOf;
use PHPUnit\Framework\TestCase;

class NumberTest extends TestCase
{
    final public function testExecute(): void
    {
        $expression = new Number(5);

        $result = $expression->execute();

        assertInstanceOf(Number::class, $result);
        assertEquals(5, $result->toFloat());
        assertEquals(5, $expression->toFloat());
    }
}
