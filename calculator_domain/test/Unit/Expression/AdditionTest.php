<?php

namespace Calculator\DomainTest\Unit\Expression;

use Calculator\Domain\Expression\Addition;
use Calculator\Domain\Expression\Exception\WrongValueException;
use Calculator\Domain\Expression\Number;
use Calculator\Domain\Expression\UnsignedNumber;
use function PHPUnit\Framework\assertEquals;
use function PHPUnit\Framework\assertInstanceOf;
use PHPUnit\Framework\TestCase;

class AdditionTest extends TestCase
{
    /**
     * @throws WrongValueException
     */
    final public function testExecute(): void
    {
        $expression = new Addition(new UnsignedNumber(2), new UnsignedNumber(3));

        $result = $expression->execute();

        assertInstanceOf(Number::class, $result);
        assertEquals(5, $result->toFloat());
        assertEquals(5, $expression->toFloat());
    }
}
