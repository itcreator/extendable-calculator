<?php

namespace Calculator\DomainTest\Unit\Expression;

use Calculator\Domain\Expression\Exception\WrongValueException;
use Calculator\Domain\Expression\Number;
use Calculator\Domain\Expression\Subtraction;
use Calculator\Domain\Expression\UnsignedNumber;
use function PHPUnit\Framework\assertEquals;
use function PHPUnit\Framework\assertInstanceOf;
use PHPUnit\Framework\TestCase;

class SubtractionTest extends TestCase
{
    /**
     * @throws WrongValueException
     */
    final public function testExecute(): void
    {
        $expression = new Subtraction(new UnsignedNumber(3), new UnsignedNumber(2));

        $result = $expression->execute();

        assertInstanceOf(Number::class, $result);
        assertEquals(1, $result->toFloat());
        assertEquals(1, $expression->toFloat());
    }
}
