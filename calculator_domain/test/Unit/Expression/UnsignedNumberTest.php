<?php

namespace Calculator\DomainTest\Unit\Expression;

use Calculator\Domain\Expression\Exception\WrongValueException;
use Calculator\Domain\Expression\UnsignedNumber;
use function PHPUnit\Framework\assertEquals;
use function PHPUnit\Framework\assertInstanceOf;
use PHPUnit\Framework\TestCase;

class UnsignedNumberTest extends TestCase
{
    /**
     * @throws WrongValueException
     */
    final public function testExecute(): void
    {
        $expression = new UnsignedNumber(5);

        $result = $expression->execute();

        assertInstanceOf(UnsignedNumber::class, $result);
        assertEquals(5, $result->toFloat());
        assertEquals(5, $expression->toFloat());
    }

    final public function testConstructorWithNegativeValue(): void
    {
        $this->expectException(WrongValueException::class);

        new UnsignedNumber(-2);
    }
}
