<?php

namespace Calculator\DomainTest\Unit\Expression;

use Calculator\Domain\Expression\Exception\WrongValueException;
use Calculator\Domain\Expression\Multiplication;
use Calculator\Domain\Expression\Number;
use Calculator\Domain\Expression\UnsignedNumber;
use function PHPUnit\Framework\assertEquals;
use function PHPUnit\Framework\assertInstanceOf;
use PHPUnit\Framework\TestCase;

class MultiplicationTest extends TestCase
{
    /**
     * @throws WrongValueException
     */
    final public function testExecute(): void
    {
        $expression = new Multiplication(new UnsignedNumber(2), new UnsignedNumber(3));

        $result = $expression->execute();

        assertInstanceOf(Number::class, $result);
        assertEquals(6, $result->toFloat());
        assertEquals(6, $expression->toFloat());
    }
}
