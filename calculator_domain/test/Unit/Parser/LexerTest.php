<?php

namespace Calculator\DomainTest\Unit\Parser;

use Calculator\Domain\Parser\Exception\UnknownTokenException;
use Calculator\Domain\Parser\Exception\WrongTypeOfDefinitionException;
use Calculator\Domain\Parser\Lexer;
use Calculator\Domain\Parser\Token\AdditionToken;
use Calculator\Domain\Parser\Token\Definition\AdditionDefinition;
use Calculator\Domain\Parser\Token\Definition\Matcher\TokenValueMatcher;
use Calculator\Domain\Parser\Token\Definition\UIntDefinition;
use Calculator\Domain\Parser\Token\UIntToken;
use function PHPUnit\Framework\assertCount;
use function PHPUnit\Framework\assertEquals;
use function PHPUnit\Framework\assertInstanceOf;
use PHPUnit\Framework\TestCase;

class LexerTest extends TestCase
{
    /**
     * @throws WrongTypeOfDefinitionException
     * @throws UnknownTokenException
     */
    final public function testTokenization(): void
    {
        $matcher = new TokenValueMatcher();
        $lexer = new Lexer([
            new UIntDefinition($matcher),
            new AdditionDefinition($matcher),
        ]);

        $tokens = $lexer->tokenize('1');

        assertCount(1, $tokens);
        assertInstanceOf(UIntToken::class, $tokens[0]);
        assertEquals('1', $tokens[0]->getValue());

        $tokens = $lexer->tokenize('2+3');

        assertCount(3, $tokens);
        assertInstanceOf(UIntToken::class, $tokens[0]);
        assertEquals('2', $tokens[0]->getValue());
        assertInstanceOf(AdditionToken::class, $tokens[1]);
        assertEquals('+', $tokens[1]->getValue());
        assertInstanceOf(UIntToken::class, $tokens[2]);
        assertEquals('3', $tokens[2]->getValue());
    }

    /**
     * @throws WrongTypeOfDefinitionException
     */
    final public function testTokenizeThrowsWrongTypeOfDefinitionException(): void
    {
        $this->expectException(WrongTypeOfDefinitionException::class);

        /* @phpstan-ignore-next-line */
        new Lexer([
            new UIntDefinition(new TokenValueMatcher()),
            'Someone that doesn\'t implement DefinitionInterface',
        ]);
    }

    /**
     * @throws WrongTypeOfDefinitionException
     */
    final public function testTokenizeThrowsUnknownTokenException(): void
    {
        $matcher = new TokenValueMatcher();
        $lexer = new Lexer([
            new UIntDefinition($matcher),
            new AdditionDefinition($matcher),
        ]);

        $this->expectException(UnknownTokenException::class);
        $this->expectExceptionCode(1);
        $this->expectExceptionMessage('Unknown token. Expression: 1-2, offset: 1');

        $lexer->tokenize('1-2');
    }
}
