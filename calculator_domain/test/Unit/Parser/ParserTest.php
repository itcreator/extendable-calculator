<?php

namespace Calculator\DomainTest\Unit\Parser;

use Calculator\Domain\Expression\Addition;
use Calculator\Domain\Expression\Multiplication;
use Calculator\Domain\Expression\NegativeNumber;
use Calculator\Domain\Expression\Subtraction;
use Calculator\Domain\Expression\UnsignedNumber;
use Calculator\Domain\Parser\Parser;
use Calculator\Domain\Parser\Token\AdditionToken;
use Calculator\Domain\Parser\Token\MultiplicationToken;
use Calculator\Domain\Parser\Token\SubtractionToken;
use Calculator\Domain\Parser\Token\UIntToken;
use function PHPUnit\Framework\assertEquals;
use function PHPUnit\Framework\assertInstanceOf;
use PHPUnit\Framework\TestCase;

class ParserTest extends TestCase
{
    final public function testParseExpressionWithOneBinaryOperation(): void
    {
        $parser = new Parser();

        $result = $parser->parse([
            new UIntToken('3'),
            new SubtractionToken('-'),
            new UIntToken('4'),
        ]);

        assertInstanceOf(Subtraction::class, $result);
        $reflection = new \ReflectionClass($result);
        /** @var NegativeNumber $first */
        $first = $reflection->getProperty('first')->getValue($result);
        /** @var NegativeNumber $second */
        $second = $reflection->getProperty('second')->getValue($result);

        assertInstanceOf(UnsignedNumber::class, $first);
        assertEquals(3, $first->toFloat());

        assertInstanceOf(UnsignedNumber::class, $second);
        assertEquals(4, $second->toFloat());
    }

    final public function testParseExpressionWithOneBinaryOperationAndNegativeValue(): void
    {
        $parser = new Parser();

        $result = $parser->parse([
            new SubtractionToken('-'),
            new UIntToken('3'),
            new MultiplicationToken('*'),
            new SubtractionToken('-'),
            new UIntToken('4'),
        ]);

        assertInstanceOf(Multiplication::class, $result);
        $reflection = new \ReflectionClass($result);
        /** @var NegativeNumber $first */
        $first = $reflection->getProperty('first')->getValue($result);
        /** @var NegativeNumber $second */
        $second = $reflection->getProperty('second')->getValue($result);

        assertInstanceOf(NegativeNumber::class, $first);
        assertEquals(-3, $first->toFloat());

        assertInstanceOf(NegativeNumber::class, $second);
        assertEquals(-4, $second->toFloat());
    }

    final public function testParseExpressionWithPrioritisation(): void
    {
        $parser = new Parser();

        $result = $parser->parse([
            new UIntToken('3'),
            new MultiplicationToken('*'),
            new UIntToken('4'),
            new AdditionToken('+'),
            new UIntToken('2'),
        ]);

        assertInstanceOf(Addition::class, $result);
    }
}
