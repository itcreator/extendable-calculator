<?php

namespace Calculator\DomainTest\Unit\Parser\Token;

use Calculator\Domain\Expression\Multiplication;
use Calculator\Domain\Parser\Token\MultiplicationToken;
use function PHPUnit\Framework\assertEquals;
use PHPUnit\Framework\TestCase;

class MultiplicationTokenTest extends TestCase
{
    final public function testMatch(): void
    {
        $definition = new MultiplicationToken('*');

        assertEquals(20, $definition->getPrecedence());
        assertEquals('*', $definition->getValue());
        assertEquals(Multiplication::class, $definition->getExpressionClass());
    }
}
