<?php

namespace Calculator\DomainTest\Unit\Parser\Token;

use Calculator\Domain\Expression\Addition;
use Calculator\Domain\Parser\Token\AdditionToken;
use function PHPUnit\Framework\assertEquals;
use PHPUnit\Framework\TestCase;

class AdditionTokenTest extends TestCase
{
    final public function testMatch(): void
    {
        $definition = new AdditionToken('+');

        assertEquals(10, $definition->getPrecedence());
        assertEquals('+', $definition->getValue());
        assertEquals(Addition::class, $definition->getExpressionClass());
    }
}
