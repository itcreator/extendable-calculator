<?php

namespace Calculator\DomainTest\Unit\Parser\Token;

use Calculator\Domain\Expression\UnsignedNumber;
use Calculator\Domain\Parser\Token\UIntToken;
use function PHPUnit\Framework\assertEquals;
use PHPUnit\Framework\TestCase;

class UIntTokenTest extends TestCase
{
    final public function testMatch(): void
    {
        $definition = new UIntToken('11');

        assertEquals('11', $definition->getValue());
        assertEquals(UnsignedNumber::class, $definition->getExpressionClass());
    }
}
