<?php

namespace Calculator\DomainTest\Unit\Parser\Token;

use Calculator\Domain\Expression\Subtraction;
use Calculator\Domain\Parser\Token\SubtractionToken;
use function PHPUnit\Framework\assertEquals;
use PHPUnit\Framework\TestCase;

class SubtractionTokenTest extends TestCase
{
    final public function testMatch(): void
    {
        $definition = new SubtractionToken('-');

        assertEquals(10, $definition->getPrecedence());
        assertEquals('-', $definition->getValue());
        assertEquals(Subtraction::class, $definition->getExpressionClass());
    }
}
