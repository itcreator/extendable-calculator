<?php

namespace Calculator\DomainTest\Unit\Parser\Token;

use Calculator\Domain\Expression\NegativeNumber;
use Calculator\Domain\Parser\Token\NegativeNumberToken;
use function PHPUnit\Framework\assertEquals;
use PHPUnit\Framework\TestCase;

class NegativeNumberTokenTest extends TestCase
{
    final public function testMatch(): void
    {
        $definition = new NegativeNumberToken('11');

        assertEquals('11', $definition->getValue());
        assertEquals(NegativeNumber::class, $definition->getExpressionClass());
    }
}
