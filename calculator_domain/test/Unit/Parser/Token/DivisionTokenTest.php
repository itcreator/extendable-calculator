<?php

namespace Calculator\DomainTest\Unit\Parser\Token;

use Calculator\Domain\Expression\Division;
use Calculator\Domain\Parser\Token\DivisionToken;
use function PHPUnit\Framework\assertEquals;
use PHPUnit\Framework\TestCase;

class DivisionTokenTest extends TestCase
{
    final public function testMatch(): void
    {
        $definition = new DivisionToken('/');

        assertEquals(20, $definition->getPrecedence());
        assertEquals('/', $definition->getValue());
        assertEquals(Division::class, $definition->getExpressionClass());
    }
}
