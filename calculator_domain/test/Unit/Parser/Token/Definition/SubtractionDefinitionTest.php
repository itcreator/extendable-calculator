<?php

namespace Calculator\DomainTest\Unit\Parser\Token\Definition;

use Calculator\Domain\Parser\Token\Definition\Matcher\TokenMatchingException;
use Calculator\Domain\Parser\Token\Definition\Matcher\TokenValueMatcher;
use Calculator\Domain\Parser\Token\Definition\SubtractionDefinition;
use Calculator\Domain\Parser\Token\SubtractionToken;
use function PHPUnit\Framework\assertInstanceOf;
use PHPUnit\Framework\TestCase;

class SubtractionDefinitionTest extends TestCase
{
    /**
     * @throws TokenMatchingException
     */
    final public function testMatch(): void
    {
        $definition = new SubtractionDefinition(new TokenValueMatcher());

        assertInstanceOf(SubtractionToken::class, $definition->match('-12'));
    }
}
