<?php

namespace Calculator\DomainTest\Unit\Parser\Token\Definition;

use Calculator\Domain\Parser\Token\Definition\DivisionDefinition;
use Calculator\Domain\Parser\Token\Definition\Matcher\TokenMatchingException;
use Calculator\Domain\Parser\Token\Definition\Matcher\TokenValueMatcher;
use Calculator\Domain\Parser\Token\DivisionToken;
use function PHPUnit\Framework\assertInstanceOf;
use PHPUnit\Framework\TestCase;

class DivisionDefinitionTest extends TestCase
{
    /**
     * @throws TokenMatchingException
     */
    final public function testMatch(): void
    {
        $definition = new DivisionDefinition(new TokenValueMatcher());

        assertInstanceOf(DivisionToken::class, $definition->match('/12'));
    }
}
