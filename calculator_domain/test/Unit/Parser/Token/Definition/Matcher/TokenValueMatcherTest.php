<?php

namespace Calculator\DomainTest\Unit\Parser\Token\Definition\Matcher;

use Calculator\Domain\Parser\Token\Definition\Matcher\TokenMatchingException;
use Calculator\Domain\Parser\Token\Definition\Matcher\TokenValueMatcher;
use function PHPUnit\Framework\assertEquals;
use function PHPUnit\Framework\assertFalse;
use PHPUnit\Framework\TestCase;

class TokenValueMatcherTest extends TestCase
{
    /**
     * @throws TokenMatchingException
     */
    final public function testSuccessfullyMatching(): void
    {
        $matcher = new TokenValueMatcher();
        $result = $matcher->match('-123', '/\-/');

        assertEquals('-', $result);

        $result = $matcher->match('123+3', '/\d+/');

        assertEquals('123', $result);
    }

    /**
     * @throws TokenMatchingException
     */
    final public function testMatchingReturnsFalse(): void
    {
        $matcher = new TokenValueMatcher();
        $result = $matcher->match('23-123', '/\-/');

        assertFalse($result);

        $result = $matcher->match('-pi', '/\d+/');

        assertFalse($result);
    }

    /**
     * @throws TokenMatchingException
     */
    final public function testMatchingFailedWithException(): void
    {
        $this->expectException(TokenMatchingException::class);
        $matcher = new TokenValueMatcher();
        $matcher->match('23-123', '/');
    }
}
