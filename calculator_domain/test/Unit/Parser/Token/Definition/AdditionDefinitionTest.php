<?php

namespace Calculator\DomainTest\Unit\Parser\Token\Definition;

use Calculator\Domain\Parser\Token\AdditionToken;
use Calculator\Domain\Parser\Token\Definition\AdditionDefinition;
use Calculator\Domain\Parser\Token\Definition\Matcher\TokenMatchingException;
use Calculator\Domain\Parser\Token\Definition\Matcher\TokenValueMatcher;
use function PHPUnit\Framework\assertInstanceOf;
use PHPUnit\Framework\TestCase;

class AdditionDefinitionTest extends TestCase
{
    /**
     * @throws TokenMatchingException
     */
    final public function testMatch(): void
    {
        $definition = new AdditionDefinition(new TokenValueMatcher());

        assertInstanceOf(AdditionToken::class, $definition->match('+12'));
    }
}
