<?php

namespace Calculator\DomainTest\Unit\Parser\Token\Definition;

use Calculator\Domain\Parser\Token\Definition\Matcher\TokenMatchingException;
use Calculator\Domain\Parser\Token\Definition\Matcher\TokenValueMatcher;
use Calculator\Domain\Parser\Token\Definition\MultiplicationDefinition;
use Calculator\Domain\Parser\Token\MultiplicationToken;
use function PHPUnit\Framework\assertInstanceOf;
use PHPUnit\Framework\TestCase;

class MultiplicationDefinitionTest extends TestCase
{
    /**
     * @throws TokenMatchingException
     */
    final public function testMatch(): void
    {
        $definition = new MultiplicationDefinition(new TokenValueMatcher());

        assertInstanceOf(MultiplicationToken::class, $definition->match('*12'));
    }
}
