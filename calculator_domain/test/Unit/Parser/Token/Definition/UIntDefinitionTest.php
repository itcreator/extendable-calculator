<?php

namespace Calculator\DomainTest\Unit\Parser\Token\Definition;

use Calculator\Domain\Parser\Token\Definition\Matcher\TokenMatchingException;
use Calculator\Domain\Parser\Token\Definition\Matcher\TokenValueMatcher;
use Calculator\Domain\Parser\Token\Definition\UIntDefinition;
use Calculator\Domain\Parser\Token\UIntToken;
use function PHPUnit\Framework\assertEquals;
use function PHPUnit\Framework\assertInstanceOf;
use PHPUnit\Framework\TestCase;

class UIntDefinitionTest extends TestCase
{
    /**
     * @throws TokenMatchingException
     */
    final public function testMatch(): void
    {
        $definition = new UIntDefinition(new TokenValueMatcher());
        /** @var UIntToken $result */
        $result = $definition->match('12+3');

        assertInstanceOf(UIntToken::class, $result);
        assertEquals('12', $result->getValue());
    }
}
